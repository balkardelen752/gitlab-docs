###############################################
#        Build and deploy the website         #
###############################################

.build_base:
  stage: build
  extends:
    - .retry
    - .bundle_and_yarn
  artifacts:
    paths:
      - public
    expire_in: 1d

# Common setup for review apps
.review-environment:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:www-gitlab-com-debian-bullseye-ruby-3.0-node-16
  environment:
    name: review/$CI_COMMIT_REF_SLUG$REVIEW_SLUG
    url: https://$CI_COMMIT_REF_SLUG$REVIEW_SLUG.docs.gitlab-review.app
    on_stop: review_stop
    auto_stop_in: 30 days

#
# The script lines for compiling and minifying the site
#
.compile-and-minify-scripts:
  script:
    - bundle exec rake default
    - bundle exec nanoc compile -VV
    # Create _redirects for Pages redirects
    - bundle exec rake redirects
    # Build the Lunr.js search index if needed
    - if [[ "$ALGOLIA_SEARCH" == "false" ]]; then node scripts/lunr/preindex.js; fi
    # Calculate sizes before and after minifying/gzipping the static files (HTML, CSS, JS)
    - SIZE_BEFORE_MINIFY=$(du -sh public/ | awk '{print $1}')
    # Minify the assets of the resulting site
    - cd public
    - ../scripts/minify-assets.sh ./ ./
    - cd ..
    - SIZE_AFTER_MINIFY=$(du -sh public/ | awk '{print $1}')
    # Print size results
    - echo -e "Size before minifying ......... $SIZE_BEFORE_MINIFY\nSize after minifying ................... $SIZE_AFTER_MINIFY"

#
# The script lines for gzipping the site (not used in upstream review apps, to speed up pipelines)
#
.gzip-scripts:
  script:
    # Use gzip to compress static content for faster web serving. Keep uncompressed files for browsers that don't support
    # compressed files: https://docs.gitlab.com/ee/user/project/pages/introduction.html#serving-compressed-assets
    - find public/ -type f \( -iname "*.html" -o -iname "*.js"  -o -iname "*.css"  -o -iname "*.svg" -o -iname "*.json" \) -exec gzip --keep  --best --force --verbose {} \;
    - SIZE_AFTER_GZIP=$(du -sh public/ | awk '{print $1}')
    # Print size results
    - echo -e "Size after adding gzipped versions ..... $SIZE_AFTER_GZIP"

#
# Compile "prod" only on the default and stable branches
#
compile_prod:
  extends:
    - .rules_prod
    - .build_base
  variables:
    ALGOLIA_SEARCH: 'true'
    NANOC_ENV: 'production'
  script:
    - !reference [".compile-and-minify-scripts", "script"]
    - !reference [".gzip-scripts", "script"]

#
# Compile "dev" for all MRs in `gitlab-docs`
#
compile_dev:
  extends:
    - .rules_dev
    - .build_base
  variables:
    ALGOLIA_SEARCH: 'false'
  script:
    - !reference [".compile-and-minify-scripts", "script"]
    - !reference [".gzip-scripts", "script"]

#
# Compile but skip gzip step for review apps from upstream projects
#
compile_upstream_review_app:
  extends:
    - .rules_upstream_review_app
    - .build_base
  variables:
    ALGOLIA_SEARCH: 'false'
  script:
    - !reference [".compile-and-minify-scripts", "script"]

###############################################
#               Review Apps                   #
###############################################

#
# Deploy the Review App on a GCS bucket.
#
review:
  stage: deploy
  extends:
    - .retry
    - .review-environment
  before_script: []
  cache: {}
  script:
    - scripts/review-replace-urls.sh
    - scripts/deploy-review-app.sh
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-renovate-forks/gitlab-docs"'
      when: manual
    - if: '$CI_PROJECT_PATH !~ /^gitlab-org/'
      when: never
    - if: '$CI_MERGE_REQUEST_ID'
    - if: '$CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "trigger"'
    - if: '$CI_COMMIT_BRANCH =~ /docs-preview/'  # TODO: Remove once no projects create such branch

#
# Stop the Review App
#
review_stop:
  stage: deploy
  extends:
    - .retry
    - .review-environment
  variables:
    DEPLOY_DELETE_APP: 'true'
  environment:
    action: stop
  needs: []
  artifacts: {}
  before_script: []
  cache: {}
  script:
    - scripts/deploy-review-app.sh
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-renovate-forks/gitlab-docs"'
      allow_failure: true
      when: manual
    - if: '$CI_PROJECT_PATH !~ /^gitlab-org/'
      when: never
    - if: '$CI_MERGE_REQUEST_ID || $CI_PIPELINE_SOURCE == "pipeline"|| $CI_PIPELINE_SOURCE == "trigger"'
      allow_failure: true
      when: manual
    # TODO: Remove once no projects create such branch
    - if: '$CI_COMMIT_BRANCH =~ /docs-preview/'
      allow_failure: true
      when: manual

#
# Clean up stopped review app environments. Done once a month in a scheduled pipeline,
# only deletes stopped environments that are over 30 days old.
#
delete_stopped_environments:
  image: alpine:latest
  needs: []
  before_script: []
  dependencies: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $PIPELINE_SCHEDULE_TIMING == "monthly"
  stage: test
  script:
    - apk --update add curl jq
    - curl --request DELETE "https://gitlab.com/api/v4/projects/1794617/environments/review_apps?limit=1000&dry_run=false&private_token=$DELETE_ENVIRONMENTS_TOKEN" | jq

###############################################
#          GitLab Pages (production)          #
###############################################

#
# Deploy to production with GitLab Pages
#
pages:
  resource_group: pages
  extends:
    - .rules_pages
    - .retry
  image: registry.gitlab.com/gitlab-org/gitlab-docs:latest
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script: []
  cache: {}
  environment:
    name: production
    url: https://docs.gitlab.com
  # We are using dependencies, because we do not want to
  # re-deploy if the previous stages failed.
  dependencies:
    - compile_prod    # Contains the public directory
  script:
    #
    # We want to use the artifacts of the compile_prod job as
    # the latest docs deployment, and the other versions are
    # taken from /usr/share/nginx/html which are included in
    # the image we pull from.
    #
    - mv /usr/share/nginx/html/1* public/
  artifacts:
    paths:
      - public
    expire_in: 1d
